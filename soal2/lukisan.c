#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<signal.h>
#include<unistd.h>
#include<syslog.h>
#include<fcntl.h>
#include<errno.h>
#include<time.h>
#include<wait.h>
#include<sys/prctl.h>
#include<sys/types.h>
#include<sys/stat.h>

//membuat killer.c, ketentuan mode a dan b
void killer(bool mode, pid_t sid){
	
	//akses write ke file
    FILE *filekill = fopen("killer.c", "w+");
    
    //header yang digunakan pada killer.c
    fprintf(filekill, "#include <stdio.h>\n");
    fprintf(filekill, "#include <stdlib.h>\n");
    fprintf(filekill, "#include <unistd.h>\n");
    fprintf(filekill, "#include <wait.h>\n");
    
    //main function
    fprintf(filekill, "int main() {\n");
    
    	//fork dahulu untuk melihat proses, apabila 0 cek argumen mode
	    fprintf(filekill, "pid_t child_id = fork();\n");
	    fprintf(filekill, "if (child_id == 0) {\n");
	    
	    // MODE_A
	    /*Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. (pakai pkill)*/
	    /*pkill" yang dijalankan akan memberikan sinyal "SIGKILL" (-9) kepada proses-proses yang memiliki id grup (session) sama dengan "sid". Sinyal 
		"SIGKILL" akan memaksa proses untuk segera berhenti.*/
	    char mode_arg[100];
	    if (mode == true) {
	        fprintf(filekill, "execl(\"/usr/bin/pkill\", \"pkill\", \"-9\", \"-s\", \"%d\", NULL);\n", sid);
    	}
    	
    	// MODE_B
    	/*Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi 
		membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete) (kill pid aja, ).*/
		/*
		"kill" akan memberikan sinyal "SIGKILL" (-9) kepada proses yang 
		memiliki pid yang sama dengan nilai yang diberikan oleh getpid().
		*/
	    else if (mode == false) {
	        fprintf(filekill, "execl(\"/bin/kill\", \"kill\", \"-9\", \"%d\", NULL);\n", getpid());        
	    }
	    
		    fprintf(filekill, "}\n");
		fprintf(filekill, "while(wait(NULL) > 0);\n");
		
		/*
		while(wait(NULL) > 0)

		loop untuk menunggu child process selesai dieksekusi.
		parameter "NULL" menandakan status akhir child process tidak berpengaruh kepada while, karena
		esensinya kita hanya menunggu untuk child process selesai dieksekusi.
		Jika nilai return wait(NULL) > 0, 
		maka masih ada child process yang sedang dieksekusi.
		*/


		/*generate sebuah program "killer" yang siap di run(executable)
		 untuk menterminasi semua operasi program tersebut. 
		 Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.*/
		fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n");
	fprintf(filekill, "return 0; }\n");

    fclose(filekill);
	
	int flag = 0;
    // compile killer.c
    pid_t child_id = fork();
    if(child_id == 0){
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
	waitpid(child_id, &flag, 0);

    // hapus killer.c
    child_id = fork();
    if (child_id == 0) {
    	execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
    
    //setelah dicompile akan membentuk file killer, lalu file killer.c dihapus. apabila killer dijalankan maka file killer akan dihapus.
    
}

int main(int argc, char* argv[]){
	
    // Check argument
	bool mode;
	if(argc == 1){
		printf("Gunakan argumen -a atau -b\n");
		exit(0);
	}
    if(strcmp("-a", argv[1]) == 0) {
    	printf("MODE_A\n");
        mode = true;
    } 
	else if (strcmp("-b", argv[1]) == 0) {
		printf("MODE_B\n");
        mode = false;
    } 
	else {
        printf("Inputkan mode yang benar (gunakan -a atau -b)\n");
        exit(1);
    }
    
    // template daemon di modul
	pid_t pid, sid;   // Variabel untuk menyimpan PID
	pid = fork();     // Menyimpan PID dari Child Process

	/* Keluar saat fork gagal
	* (nilai variabel pid < 0) */
	if (pid < 0) {
		exit(EXIT_FAILURE);
  	}

	/* Keluar saat fork berhasil
	* (nilai variabel pid adalah PID dari child process) */
	if (pid > 0) {
	   exit(EXIT_SUCCESS);
	}

	umask(0);

  	sid = setsid();
  	if (sid < 0) {
    	exit(EXIT_FAILURE);
  	}

//  	if ((chdir("/")) < 0) {
//    	exit(EXIT_FAILURE);
//  	}
  	
	// Check program mode
	killer(mode, sid);
  	close(STDIN_FILENO);
  	close(STDOUT_FILENO);
  	close(STDERR_FILENO);

    while(1)
    {
        
        char foldernm[20];
		time_t ctime = time(NULL);
		struct tm* ttime = localtime(&ctime);
	    strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
       
        pid_t pid = fork();
	    if(pid == 0){
	    	//membuat folder dengan ketentuan
	    	mkdir(foldernm, 0777);
	    	
	    	//mendownload gambar
	    	pid_t down;
		    char imagenm[25], 
				 loc[50], 
				 link[50];
		   
		   	//loop sebanyak 15x, untuk mendapat gambar 15 setiap foldernya. 
		    for(int i = 0; i < 15; i++){
		        down = fork();
		        if(down == 0){
		            time_t ctime2 = time(NULL);
		            struct tm* ttime2 = localtime(&ctime2);
		            strftime(imagenm, sizeof(imagenm), "%Y-%m-%d_%H:%M:%S.jpg", ttime2);
		            sprintf(loc, "%s/%s", foldernm, imagenm);
		            sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
		            execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
		        }
		        sleep(5);
		    }
		    while(wait(NULL) > 0);
		    
		    //zip folder
		    pid_t zip = fork();
		    if(zip == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldernm, foldernm, NULL);
            }
            int statuszip = 0;
            /*
			untuk menunggu child process tertentu selesai dieksekusi.
			parameter 1:  menentukan child process mana yang ingin diwait. 
				Jika nilai pid=-1, maka fungsi waitpid() akan menunggu child process apa saja yang selesai dieksekusi. 
				Jika nilai pid>0, maka fungsi waitpid() akan menunggu child process dengan id yang sama dengan nilai pid. 
				Jika nilai pid==0, maka fungsi waitpid() akan menunggu child process yang memiliki id grup yang sama dengan proses panggilan.
			parameter 2: pointer yang menunjuk ke variabel integer dimana status akhir dari proses anak akan disimpan.
			parameter 3: parameter ini menentukan opsi untuk perilaku fungsi waitpid(). Jika nilai options=0, maka fungsi waitpid() akan berjalan secara default.
			*/
		    waitpid(zip, &statuszip, 0);
            
            //remove folder setelah zip
            /*Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, 
			folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).*/
            execl("/usr/bin/rm", "rm", "-r", foldernm, NULL);
		}
		
        sleep(30);
    }
}

# Lapres Praktikum Modul 1 Kelompok D07
Anggota Kelompok ''D07'' 
| Nama                      | NRP        |
|---------------------------|------------|
| Hanun Shaka Puspa         | 5025211051 |
| Mavaldi Rizqy Hazdi       | 5025211086 |
| Daffa Saskara             | 5025201249 |

## 1. Binatang
Diminta untuk mengerjakan beberapa tugas dengan urutan berikut :
1. Download zip file berisi kumpulan foto binatang
2. Unzip file tersebut
3. Memilah gambar kemudian dipindahkan ke direktori __HewanDarat__, __HewanAmphibi__, dan __HewanAir__.
4. Zip tiap direktori yang sudah dipilah, lalu hapus direktorinya
5. Pilih nama hewan random

### Penjelasan Solusi
Pertama-tama, dicek apakah direktori target sudah ada menggunakan ```DIR *dir = opendir(path);```. Jika belum (value dir berisi _null_), direktori target akan dibuat menggunakan perintah berikut:
```
  if (dir) {
    printf("Zoo Shift Directory Already Exists\n");
    closedir(dir);
  } else {
    printf("Directory does not exist, creating folder ...\n");
    int status = mkdir(path, 0777);

    if (status == 0) {
      printf("Zoo Shift Directory Created\n");
    } else {
      printf("Zoo Shift Directory Failed to Create\n");
    }
  }
```
Lalu untuk melakukan download, digunakan _command_ ```wget``` yang dipanggil dalam ```system()```. Sebelum dipanggil, command dengan path target dan url download disusun dengan ```strcat()```.
```
char command[200] = "wget -O ";
strcat(command, file);
strcat(command, " ");
strcat(command, url);
system(command);
```
Setelah itu, file yang sudah di-download di-unzip menggunakan command ```execlp()```. Karena command tersebut men-terminate program setelah dipanggil, maka perlu dibuat child proses untuk menjalankannya supaya program utama tetap berjalan.
```
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    int unzip_status = execlp("unzip", "unzip", "-q", file, "-d", path, (char*)NULL);
    if (unzip_status == -1) {
      perror("Error unzipping file");
      exit(1);
    }
  } else {
    while ((wait(&status)) > 0);
  }
```
Sebelum melakukan pemilahan file, dibuat direktori baru untuk tiap kategori binatang. Dibuat array berisi nama-nama direktori tersebut. Untuk tiap nama tersebut, dibuat direktori baru menggunakan ```mkdir```.
```
  char *animalTypes[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
  int animalTypesLength = sizeof(animalTypes) / sizeof(animalTypes[0]);

  for (int i = 0; i < animalTypesLength; i++) {
    char *animalType = animalTypes[i];
    char animalTypePath[200] = "./zoo_shift/";
    strcat(animalTypePath, animalType);
    DIR *animalTypeDir = opendir(animalTypePath);

    if (animalTypeDir) {
      printf("Directory %s already exists\n", animalType);
      closedir(animalTypeDir);
    } else {
      printf("Directory %s does not exist, creating folder ...\n", animalType);
      int status = mkdir(animalTypePath, 0777);

      if (status == 0) {
        printf("Directory %s created\n", animalType);
      } else {
        printf("Directory %s failed to create\n", animalType);
      }
    }
  }
```
Kemudian untuk melakukan pemindahan file, perlu dilakukan beberapa step berikut:
1. Mengecek lalu membuka direktori tempat file-file binatang.
```
  struct dirent *de;
  DIR *dr = opendir("./zoo_shift/");

  if (dr == NULL) {
    printf("Could not open current directory" );
    return 0;
  }
```

2. Mendapatkan nama-nama setiap file lalu menutup direktori
```
while ((de = readdir(dr)) != NULL) {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
      continue;
    }

    // skip if file name starts with "Hewan"
    if (strncmp(de->d_name, "Hewan", 5) == 0) {
      continue;
    }

    // skip if file name after dot is zip
    if (strcmp(strchr(de->d_name, '.') + 1, "zip") == 0) {
      continue;
    }

    strcpy(file_names[file_count], de->d_name);
    file_count++;

    printf("Found file: %s\n", de->d_name); 
}

closedir(dr);
```

3. Untuk tiap nama file, dapatkan kategorinya dari setelah _underscore_ lalu pindahkan ke direktori yang bersangkutan.
```
for (int i = 0; i < file_count; i++) {
    char *fileName = file_names[i];
    char *animalType = strchr(fileName, '_') + 1;
    animalType = strtok(animalType, ".");

    printf("Moving %s to %s ...\n", fileName, animalType);

    if (strcmp(animalType, "darat") == 0) {
      char command[200] = "mv ./zoo_shift/";
      strcat(command, fileName);
      strcat(command, ".jpg");
      strcat(command, " ./zoo_shift/HewanDarat/");
      system(command);
    } else if (strcmp(animalType, "amphibi") == 0) {
      char command[200] = "mv ./zoo_shift/";
      strcat(command, fileName);
      strcat(command, ".jpg");
      strcat(command, " ./zoo_shift/HewanAmphibi/");
      system(command);
    } else if (strcmp(animalType, "air") == 0) {
      char command[200] = "mv ./zoo_shift/";
      strcat(command, fileName);
      strcat(command, ".jpg");
      strcat(command, " ./zoo_shift/HewanAir/");
      system(command);
    }
  }
```

Setelah itu, dilakukan zip file dengan command ```execlp```. Step tersebut dilakukan untuk setiap direktori target pemilahan file maka perlu dibuat for loop yang di dalamnya melakukan spawning child untuk mengeksekusi ```execlp``` dengan command ```zip```.
```
  for (int i = 0; i < animalTypesLength; i++) {
    char *animalType = animalTypes[i];
    char animalTypePath[200] = "./zoo_shift/";
    char destination[200] = "./zoo_shift/";
    strcat(destination, animalType);
    strcat(animalTypePath, animalType);

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
      int zip_status = execlp("zip", "zip", "-q", "-r", destination, animalTypePath, (char*)NULL);
      if (zip_status == -1) {
        perror("Error zipping file");
        exit(1);
      }
    } else {
      while ((wait(&status)) > 0);
    }
  }
```
Mirip seperti step sebelumnya yang menggunakan ```for loop``` dan ```execlp``` dalam child process, dilakukan penghapusan direktori pemilahan file. Namun di step ini digunakan command ```rm``` dengan flag ```-r``` atau rekursif untuk menghapus direktori.
```
for (int i = 0; i < animalTypesLength; i++) {
    char *animalType = animalTypes[i];
    char animalTypePath[200] = "./zoo_shift/";
    strcat(animalTypePath, animalType);

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
      int remove_status = execlp("rm", "rm", "-r", animalTypePath, (char*)NULL);
      if (remove_status == -1) {
        perror("Error removing directory");
        exit(1);
      }
    } else {
      while ((wait(&status)) > 0);
    }
  }
```
Terakhir, didapatkan nama random dari array nama hewan menggunakan ```srand()``` dan hasilnya dicetak di console.
```
  srand(time(0));
  int randomIndex = rand() % file_count;
  char *randomAnimalName = file_names[randomIndex];
  char *animalName = strtok(randomAnimalName, "_");

  printf("\nRANDOM ANIMAL NAME: %s\n\n", animalName);
```

### Screenshot Solusi
1. Tampilan console<br/>
<img src="https://i.ibb.co/VmbXn4B/Whats-App-Image-2023-04-08-at-02-09-49.jpg" width = "500"/><br/>
2. Isi zip binatang air<br/>
<img src="https://i.ibb.co/LnHLmn3/Whats-App-Image-2023-04-08-at-02-11-32.jpg" width = "500"/><br/>
3. Isi zip binatang amphibi<br/>
<img src="https://i.ibb.co/5KP7chC/Whats-App-Image-2023-04-08-at-02-11-49.jpg" width = "500"/><br/>
4. Isi zip binatang darat<br/>
<img src="https://i.ibb.co/f4YMbyz/Whats-App-Image-2023-04-08-at-02-12-08.jpg" width = "500"/><br/>

### Kendala Pengerjaan Soal
Kendala pada download menggunakan link drive yang diberikan, namun download berhasil setelah link drive diperpendek menggunakan link shortener dari ITS.

## 2. Lukisan
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

1. Menggunakan template daemon yang telah disediakan oleh modul.
2. Membuat fungsi kill dengan ketentuan:
    - **MODE_A**: Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan dengan menggunakan pkill. pkill" yang dijalankan akan memberikan sinyal "SIGKILL" (-9) kepada proses-proses yang memiliki id grup (session) sama dengan "sid". Sinyal "SIGKILL" akan memaksa proses untuk segera berhenti.
    -  **MODE_B**: Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai (semua folder terisi gambar, terzip lalu di delete) (kill pid). "kill" akan memberikan sinyal "SIGKILL" (-9) kepada proses yang memiliki pid yang sama dengan nilai yang diberikan oleh getpid().
3. Membuat folder dengan ketentuan ``[YYYY-MM-dd_HH:mm:ss]``. Digunakan sleep selama 30 detik setiap loopnya.
4. Melakukan loop sebanyak 15 pada setiap folder untuk mendownload 15 gambar dengan format ``[YYYY-MM-dd_HH:mm:ss]``. Gunakan perintah wget untuk melakukan pengambilan gambar. (ukuran (ctime % 1000) + 50 piksel).
5. Melakukan zip foler setelah loop sebanyak 15 selesai kita akan melakukan zip pada folder yang telah terisi 15 gambar lalu melakukan delete pada folder yang sudah di zip.


### Penjelasan Solusi
Pertama, Setiap folder akan diisi dengan 15 gambar yang diunduh dari https://picsum.photos/. Gambar akan diunduh setiap 5 detik dan akan memiliki bentuk persegi dengan ukuran (t%1000)+50 piksel, di mana 't' adalah waktu dalam detik Epoch Unix. Setiap gambar akan diberi nama berdasarkan format timestamp [YYYY-mm-dd_HH:mm:ss].
```c
while(1)
    {
        
        char foldernm[20];
		time_t ctime = time(NULL);
		struct tm* ttime = localtime(&ctime);
	    strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
       
        pid_t pid = fork();
	    if(pid == 0){
	    	//membuat folder dengan ketentuan
	    	mkdir(foldernm, 0777);
	    	
	    	//mendownload gambar
	    	pid_t down;
		    char imagenm[25], 
				 loc[50], 
				 link[50];
		   
		   	//loop sebanyak 15x, untuk mendapat gambar 15 setiap foldernya. 
		    for(int i = 0; i < 15; i++){
		        down = fork();
		        if(down == 0){
		            time_t ctime2 = time(NULL);
		            struct tm* ttime2 = localtime(&ctime2);
		            strftime(imagenm, sizeof(imagenm), "%Y-%m-%d_%H:%M:%S.jpg", ttime2);
		            sprintf(loc, "%s/%s", foldernm, imagenm);
		            sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
		            execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
		        }
		        sleep(5);
		    }
```
Lalu beikut adalah implementasi killer.c yang diterapkan di dalam kode
```c
void killer(bool mode, pid_t sid){
	
    FILE *filekill = fopen("killer.c", "w+");
    
    fprintf(filekill, "#include <stdio.h>\n");
    fprintf(filekill, "#include <stdlib.h>\n");
    fprintf(filekill, "#include <unistd.h>\n");
    fprintf(filekill, "#include <wait.h>\n");
    
    fprintf(filekill, "int main() {\n");
        
        fprintf(filekill, "pid_t child_id = fork();\n"); 
        fprintf(filekill, "if (child_id == 0) {\n");
        
        if (mode == true) {
            fprintf(filekill, "execl(\"/usr/bin/pkill\", \"pkill\", \"-9\", \"-s\", \"%d\", NULL);\n", sid);
        }
        
        else if (mode == false) {
            fprintf(filekill, "execl(\"/bin/kill\", \"kill\", \"-9\", \"%d\", NULL);\n", getpid());        
        }
    
        fprintf(filekill, "}\n");
        fprintf(filekill, "while(wait(NULL) > 0);\n");
        
        fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n");
    fprintf(filekill, "return 0; }\n");

    fclose(filekill);
    
    int flag = 0;
    
    pid_t child_id = fork();
    if(child_id == 0){
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
    waitpid(child_id, &flag, 0);
   
    child_id = fork();
    if (child_id == 0) {
    	execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
    
}

```

Penjelasan :
Fungsi killer digunakan untuk menerapkan aturan yang ditetapkan dalam soal dan disesuaikan dengan mode yang diberikan, seperti yang dijelaskan pada poin E dari soal.

Jika dijalankan dalam MODE_A, program utama akan segera menghentikan semua operasinya ketika program killer dijalankan. Sedangkan pada MODE_B, program utama akan berhenti, tetapi membiarkan proses di setiap folder yang masih berjalan hingga selesai (semua folder terisi gambar, terzip, dan dihapus).

Untuk menentukan mode A dan mode B, parameter mode digunakan dalam bentuk boolean. Jika benar, program akan membuat file killer sesuai dengan aturan MODE_A, dan jika salah, program akan membuat file killer sesuai dengan aturan MODE_B.

Pertama-tama, digunakan pointer file untuk membuat file dengan nama killer.c dengan akses w+. Kemudian, digunakan library stdio.h, stdlib.h, unistd.h, dan wait.h, dan dilanjutkan dengan fungsi main. Selanjutnya, dibuat proses child baru dan mode dijelaskan.

Jika dijalankan dalam MODE_A, program utama akan segera menghentikan semua operasinya ketika program killer dijalankan. pkill akan memberikan sinyal SIGKILL (-9) kepada semua proses yang memiliki ID grup (session) yang sama dengan "sid". Sinyal SIGKILL akan memaksa proses untuk segera berhenti.

Lalu, Agar rapi, file akan menzip ke 15 gambar pada folder dan mendelete `rm` folder yang ada seingga tersisakan file zip.
```c
while(1)
    {
        
        char foldernm[20];
        time_t ctime = time(NULL);
        struct tm* ttime = localtime(&ctime);
        strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
        
        pid_t pid = fork();
        if(pid == 0){
            mkdir(foldernm, 0777);
            
            pid_t down;
            char imagenm[25], 
            loc[50], 
            link[50];
            
            for(int i = 0; i < 15; i++){
                down = fork();
                if(down == 0){
                    time_t ctime2 = time(NULL);
                    struct tm* ttime2 = localtime(&ctime2);
                    strftime(imagenm, sizeof(imagenm), "%Y-%m-%d_%H:%M:%S.jpg", ttime2);
                    sprintf(loc, "%s/%s", foldernm, imagenm);
                    sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
                    execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
                }
                sleep(5);
            }
            while(wait(NULL) > 0);

            pid_t zip = fork();
            if(zip == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldernm, foldernm, NULL);
            }

            int statuszip = 0;
            waitpid(zip, &statuszip, 0);
            execl("/usr/bin/rm", "rm", "-r", foldernm, NULL);
        }
        sleep(30);
    }

```

Berikut merupakan full code dari `lukisan.c` :
```c
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<signal.h>
#include<unistd.h>
#include<syslog.h>
#include<fcntl.h>
#include<errno.h>
#include<time.h>
#include<wait.h>
#include<sys/prctl.h>
#include<sys/types.h>
#include<sys/stat.h>

//membuat killer.c, ketentuan mode a dan b
void killer(bool mode, pid_t sid){
	
	//akses write ke file
    FILE *filekill = fopen("killer.c", "w+");
    
    //header yang digunakan pada killer.c
    fprintf(filekill, "#include <stdio.h>\n");
    fprintf(filekill, "#include <stdlib.h>\n");
    fprintf(filekill, "#include <unistd.h>\n");
    fprintf(filekill, "#include <wait.h>\n");
    
    //main function
    fprintf(filekill, "int main() {\n");
    
    	//fork dahulu untuk melihat proses, apabila 0 cek argumen mode
	    fprintf(filekill, "pid_t child_id = fork();\n");
	    fprintf(filekill, "if (child_id == 0) {\n");
	    
	    // MODE_A
	    /*Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. (pakai pkill)*/
	    /*pkill" yang dijalankan akan memberikan sinyal "SIGKILL" (-9) kepada proses-proses yang memiliki id grup (session) sama dengan "sid". Sinyal 
		"SIGKILL" akan memaksa proses untuk segera berhenti.*/
	    char mode_arg[100];
	    if (mode == true) {
	        fprintf(filekill, "execl(\"/usr/bin/pkill\", \"pkill\", \"-9\", \"-s\", \"%d\", NULL);\n", sid);
    	}
    	
    	// MODE_B
    	/*Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi 
		membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete) (kill pid aja, ).*/
		/*
		"kill" akan memberikan sinyal "SIGKILL" (-9) kepada proses yang 
		memiliki pid yang sama dengan nilai yang diberikan oleh getpid().
		*/
	    else if (mode == false) {
	        fprintf(filekill, "execl(\"/bin/kill\", \"kill\", \"-9\", \"%d\", NULL);\n", getpid());        
	    }
	    
		    fprintf(filekill, "}\n");
		fprintf(filekill, "while(wait(NULL) > 0);\n");
		
		/*
		while(wait(NULL) > 0)

		loop untuk menunggu child process selesai dieksekusi.
		parameter "NULL" menandakan status akhir child process tidak berpengaruh kepada while, karena
		esensinya kita hanya menunggu untuk child process selesai dieksekusi.
		Jika nilai return wait(NULL) > 0, 
		maka masih ada child process yang sedang dieksekusi.
		*/


		/*generate sebuah program "killer" yang siap di run(executable)
		 untuk menterminasi semua operasi program tersebut. 
		 Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.*/
		fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n");
	fprintf(filekill, "return 0; }\n");

    fclose(filekill);
	
	int flag = 0;
    // compile killer.c
    pid_t child_id = fork();
    if(child_id == 0){
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
	waitpid(child_id, &flag, 0);

    // hapus killer.c
    child_id = fork();
    if (child_id == 0) {
    	execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
    
    //setelah dicompile akan membentuk file killer, lalu file killer.c dihapus. apabila killer dijalankan maka file killer akan dihapus.
    
}

int main(int argc, char* argv[]){
	
    // Check argument
	bool mode;
	if(argc == 1){
		printf("Gunakan argumen -a atau -b\n");
		exit(0);
	}
    if(strcmp("-a", argv[1]) == 0) {
    	printf("MODE_A\n");
        mode = true;
    } 
	else if (strcmp("-b", argv[1]) == 0) {
		printf("MODE_B\n");
        mode = false;
    } 
	else {
        printf("Inputkan mode yang benar (gunakan -a atau -b)\n");
        exit(1);
    }
    
    // template daemon di modul
	pid_t pid, sid;   // Variabel untuk menyimpan PID
	pid = fork();     // Menyimpan PID dari Child Process

	/* Keluar saat fork gagal
	* (nilai variabel pid < 0) */
	if (pid < 0) {
		exit(EXIT_FAILURE);
  	}

	/* Keluar saat fork berhasil
	* (nilai variabel pid adalah PID dari child process) */
	if (pid > 0) {
	   exit(EXIT_SUCCESS);
	}

	umask(0);

  	sid = setsid();
  	if (sid < 0) {
    	exit(EXIT_FAILURE);
  	}

//  	if ((chdir("/")) < 0) {
//    	exit(EXIT_FAILURE);
//  	}
  	
	// Check program mode
	killer(mode, sid);
  	close(STDIN_FILENO);
  	close(STDOUT_FILENO);
  	close(STDERR_FILENO);

    while(1)
    {
        
        char foldernm[20];
		time_t ctime = time(NULL);
		struct tm* ttime = localtime(&ctime);
	    strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
       
        pid_t pid = fork();
	    if(pid == 0){
	    	//membuat folder dengan ketentuan
	    	mkdir(foldernm, 0777);
	    	
	    	//mendownload gambar
	    	pid_t down;
		    char imagenm[25], 
				 loc[50], 
				 link[50];
		   
		   	//loop sebanyak 15x, untuk mendapat gambar 15 setiap foldernya. 
		    for(int i = 0; i < 15; i++){
		        down = fork();
		        if(down == 0){
		            time_t ctime2 = time(NULL);
		            struct tm* ttime2 = localtime(&ctime2);
		            strftime(imagenm, sizeof(imagenm), "%Y-%m-%d_%H:%M:%S.jpg", ttime2);
		            sprintf(loc, "%s/%s", foldernm, imagenm);
		            sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
		            execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
		        }
		        sleep(5);
		    }
		    while(wait(NULL) > 0);
		    
		    //zip folder
		    pid_t zip = fork();
		    if(zip == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldernm, foldernm, NULL);
            }
            int statuszip = 0;
            /*
			untuk menunggu child process tertentu selesai dieksekusi.
			parameter 1:  menentukan child process mana yang ingin diwait. 
				Jika nilai pid=-1, maka fungsi waitpid() akan menunggu child process apa saja yang selesai dieksekusi. 
				Jika nilai pid>0, maka fungsi waitpid() akan menunggu child process dengan id yang sama dengan nilai pid. 
				Jika nilai pid==0, maka fungsi waitpid() akan menunggu child process yang memiliki id grup yang sama dengan proses panggilan.
			parameter 2: pointer yang menunjuk ke variabel integer dimana status akhir dari proses anak akan disimpan.
			parameter 3: parameter ini menentukan opsi untuk perilaku fungsi waitpid(). Jika nilai options=0, maka fungsi waitpid() akan berjalan secara default.
			*/
		    waitpid(zip, &statuszip, 0);
            
            //remove folder setelah zip
            /*Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, 
			folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).*/
            execl("/usr/bin/rm", "rm", "-r", foldernm, NULL);
		}
		
        sleep(30);
    }
}

```

### Screenshoot Solusi
- Running dengan mode A
![run1](/uploads/28bb2ce9537acfa8985580e4c86a01d1/run1.jpg)

- Program Zip setelah terdownload 15 gambar
![run2](/uploads/367807a4ffedef4403aa819058dbae53/run2.jpg)

-  Program di Kill
![run3](/uploads/f8ffeee54ff8af8f9335123a16a95503/run3.jpg)

- Progra, dijalankan dengan mode B. (setelah di kill tetap jalan di backlground)
![run_luk_b](/uploads/695ab9f678dc0ce8aaf24fbb20c82164/run_luk_b.jpg)
### Kendala Pengerjaan

Sangat sulit dan nguli

## 3. Filter
Pada no.3 diminta oleh soal :
1. Download file yang berisi data pemain bola
2. Extract file hasil download yang bernama ```players.zip``` lalu hapus
3. Hapus pemain yang bukan dari Manchester United
4. Kelompokkan para pemain MU tersebut kedalam 4 folder sesuai posisi masing-masing ```Kiper```, ```Bek```, ```Gelandang```, dan ```Penyerang```
5. Buat kesebelasan terbaik dari para pemain tersebut lalu buat dalam file txt
### Penjelasan Solusi
- Untuk mendownload database pemain bola, dapat menggunakan ```wget``` pada child proses pertama 
```sh
char* argv[] = { "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL };
        execvp("wget", argv);
```
lalu gunakan ```execvp()``` untuk menjalankan argumen "wget" yang sudah dibuat. ```execvp()``` digunakan karena lebih simpel tanpa harus memakai ```usr/bin/```. 

Sebelum lanjut ke child proses selanjutnya, parent proses setelah child proses pertama tersebut akan memeriksa apakah proses anak sebelumnya keluar secara normal menggunakan ```WIFEXITED``` dan apakah status keluarannya adalah 0 ```WEXITSTATUS``` dengan contoh :
```sh
int a;
        waitpid(pid, &a, 0);
        if (WIFEXITED(a) && !WEXITSTATUS(a))
```
hal diatas akan sering dilakukan sampai child proses terakhir.

- Pada child proses selanjutnya akan dilakukan unzip file ```folder.zip``` dengan command ```unzip```
```sh
char* argv[] = { "unzip", "players.zip", NULL };
                execvp("unzip", argv);
```
sebelum masuk ke proses selanjutnya akan ada pengecekan seperti child proses pertama sebelumnya.

- Proses selanjutnya adalah menghapus file ```player.zip``` dengan command ```rm```
```sh
char* argv[] = { "rm", "players.zip", NULL };
                        execvp("rm", argv);
```
- Setelah itu akan dilakukan penghapusan terhadap player yang bukan dari Manchester United dengan cara menghapus file yang tidak mengandung nama "ManUtd" pada nama filenya
```sh
char* argv[] = { "sh", "-c", "find /home/rizqy/players -type f ! -name '*ManUtd*' -delete", NULL };
                                execvp("sh", argv);
```
- Kelompokkan para pemain MU tadi kedalam folder berdasarkan posisi masing-masing dengan cara menyocokkan nama file dengan posisi ```Kiper```, ```Bek```, ```Gelandang```, atau ```Penyerang``` lalu dipindahkan menggunakan ```mv```
```sh
char* argv[] = { "sh", "-c", "cd /home/rizqy/players && mv *Bek*.png Bek/ && mv *Gelandang*.png Gelandang/ && mv *Kiper*.png Kiper/ && mv *Penyerang*.png Penyerang/", NULL };
                                                execvp("sh", argv);
```
- Proses terakhir adalah memanggil fungsi ```buatTim()``` dan tentukan banyak pemain untuk masing-masing posisi dalam fungsi tersebut dengan contoh ``` buatTim(3, 4, 3);``` yang berarti 3 bek, 4 gelandang, dan 3 penyerang.

- Fungsi ```buatTim()``` berisi perintah-perintah untuk melakukan pemilihan pemain dengan rating tinggi sebanyak input. Pemilihan pemain rating tinggi tersebut dilakukan dengan melakukan sort lalu diambil ```n``` teratas menggunakan command ```head```. Salah satu contohnya adalah :
```sh
printf("%s", filename);
                                char argv_gldg[200];
                                sprintf(argv_gldg, "cd /home/rizqy/players/Gelandang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", num_gelandang, filename);
                                char* argv[] = { "sh", "-c",argv_gldg, NULL };
                                execvp("sh", argv);
```                                
### Screenshoot Solusi
- Tampilan terminal saat program dijalankan
![1](https://github.com/mavaldi/image-placeeee/blob/main/mod2-1.jpg?raw=true)
- Folder player setelah dilakukan pengelompokkan player menjadi 4 posisi
![2](https://github.com/mavaldi/image-placeeee/blob/main/mod2-2.png?raw=true)
- Salah satu folder dari pengelompokkan para pemain MU yaitu posisi gelandang
![3](https://github.com/mavaldi/image-placeeee/blob/main/mod2-3.png?raw=true)
- File ```.txt``` hasil dari fungsi ```buatTim()``` dan daftar para pemain sesuai dengan banyak pemain perposisi yang diinginkan (Kiper pasti 1)
![4](https://github.com/mavaldi/image-placeeee/blob/main/mod2-4.png?raw=true)

### Kendala Pengerjaan
- Kendala pada pengelompokkan player sesuai posisi masing-masing 

## 4. Mainan
Diminta untuk membuat program daemon yang memiliki cara kerja seperti sebuah cronjob.
### Penjelasan Solusi
Untuk mendapatkan argumen program, diberikan argumen di fungsi main sebagai berikut.
```
int argc, char *argv[]
```
Lalu, diperiksa length dari argumen yang dimasukkan. Jika tidak sesuai, maka program langsung di-terminate.
```
  if (argc != 5) {
    printf("[USAGE] ./banabil [hour] [minute] [second] [path]\n");
    exit(EXIT_FAILURE);
  }
```
Dideklarasikan variabel untuk jam, menit, detik, dan string path ke script yang akan dijalankan. Kemudian digunakan fungsi ```atoi()``` untuk mengonversi char ke integer untuk disimpan di variabel jam, menit, dan detik. Jika input "\*", maka variabel diset -1. Untuk variabel path digunakan ```strcpy()``` untuk meng-copy dari argumen.
```
  int hour;
  int minute;
  int second;
  char path[200];

  if (strcmp(argv[1], "\*") == 0) {
    hour = -1;
  } else {
    hour = atoi(argv[1]);
  }

  if (strcmp(argv[2], "\*") == 0) {
    minute = -1;
  } else {
    minute = atoi(argv[2]);
  }

  if (strcmp(argv[3], "\*") == 0) {
    second = -1;
  } else {
    second = atoi(argv[3]);
  }

  strcpy(path, argv[4]);
```
Untuk program daemon-nya menggunakan template dari github sesi lab. Di dalam fungsi ```while``` diambil waktu saat program berjalan. Variabel-variabel berikut nantinya akan digunakan untuk menghitung berapa detik program di-sleep sebelum dibangunkan kembali.
```
    time_t current_time;
    struct tm *time_info;

    time(&current_time);
    time_info = localtime(&current_time);

    int current_hour = time_info->tm_hour;
    int current_minute = time_info->tm_min;
    int current_second = time_info->tm_sec;
```
Dideklarasikan variabel untuk menyimpan hasil perhitungan waktu sleep.
```
int sleep_time = 0;
```
Untuk perhitungan waktu tidur program, jika diinput jam, menit, dan detik -1, maka contoh perhitungannya sebagai berikut.
```
    if (hour == -1) {
      if (minute == -1) {
        if (second == -1) {
          sleep_time = 1;
        } else {
          ...
        }
      }
      ...
    }
```
Dan seterusnya.

Untuk memastikan script dapat dijalankan, maka program ini harus diberi permission untuk execute terhadap script yang dimaksud. Dalam solusi ini, digunakan ```chmod``` dan ```execl``` sehingga harus di-spawn child baru.
```
    pid_t grandchild_id;
    int status;

    grandchild_id = fork();

    if (grandchild_id < 0) {
      exit(1);
    }

    if (grandchild_id == 0) {
      chmod(path, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
      execl("/bin/sh", "sh", "-c", path, (char *)NULL);
    } else {
      while ((wait(&status)) > 0);
    }
```
Untuk memastikan program berjalan dengan benar, di-spawn child yang menuliskan keterangan dalam sebuah file log bernama ```daemon_log.txt```.
```
char command[200] = "echo 'Daemon running at: ";
    char str_hour[5];
    char str_minute[5];
    char str_second[5];

    sprintf(str_hour, "%d", current_hour);
    sprintf(str_minute, "%d", current_minute);
    sprintf(str_second, "%d", current_second);

    strcat(command, str_hour);
    strcat(command, ":");
    strcat(command, str_minute);
    strcat(command, ":");
    strcat(command, str_second);
    strcat(command, "\'");
    // using local directory
    strcat(command, " >> /home/nuna/sisop-praktikum-modul-2-2023-bs-d07/soal4/daemon_log.txt");
```

Kemudian untuk mengeksekusi script, dilakukan hal yang sama yaitu meng-spawn child dan menggunakan ```execlp``` untuk mengeksekusi script.
```
    pid_t log_id;
    int log_status;

    log_id = fork();

    if (log_id < 0) {
      exit(1);
    }

    if (log_id == 0) {
      execlp("sh", "sh", "-c", command, NULL);
    } else {
      while ((wait(&log_status)) > 0);
    }
```
### Screenshoot Solusi
1. Tampilan console jika argumen program tidak sesuai<br/>
<img src="https://i.ibb.co/gdFP7yk/Whats-App-Image-2023-04-08-at-03-51-28.jpg" width="500"/><br/>
2. Tampilan console saat program dieksekusi<br/>
<img src="https://i.ibb.co/vPxD9Dd/Whats-App-Image-2023-04-08-at-03-51-37.jpg" width="500"><br/>
3. Isi file ```daemon_log.txt``` <br/>
<img src="https://i.ibb.co/vhPX3nZ/Whats-App-Image-2023-04-08-at-03-51-16.jpg" width="500"/><br/>
4. ID program saat dipanggil command ```ps aux``` <br/>
<img src="https://i.ibb.co/xf5GrRg/Whats-App-Image-2023-04-08-at-03-51-53.jpg" width="500"/><br/>

### Kendala Pengerjaan
Tidak ada kendala yang berarti pada pengerjaan soal nomor 4
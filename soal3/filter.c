#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#include <time.h>
#include <errno.h>


void buatTim(int num_bek, int num_gelandang, int num_penyerang) {
    char filename[50];
    sprintf(filename, "/home/rizqy/Formasi_%d-%d-%d.txt", num_bek, num_gelandang, num_penyerang);

    pid_t tim_pid = fork();
    if (tim_pid < 0) {
        exit(1);
    }
    else if (tim_pid == 0) {
        printf("%s", filename);
        char* argv[] = { "touch", filename, NULL };
        execvp("touch", argv);
        perror("error");
        exit(1);
    }
    else {
        int a;
        if (waitpid(tim_pid, &a, 0) < 0) {
            perror("error");
            exit(1);
        }
        if (WIFEXITED(a) && !WEXITSTATUS(a)) {
            pid_t tim_pid2 = fork();
            if (tim_pid2 < 0) {
                exit(1);
            }
            else if (tim_pid2 == 0) {
                printf("%s", filename);
                char argv_kiper[200];
                sprintf(argv_kiper, "cd /home/rizqy/players/Kiper && (ls -r1 | sort -t_ -k4 -rn | head -n 1) > %s", filename);
                char* argv[] = { "sh", "-c", argv_kiper, NULL };
                execvp("sh", argv);
                perror("error");
                exit(1);
            }
            else {
                int b;
                if (waitpid(tim_pid2, &b, 0) < 0) {
                    perror("error");
                    exit(1);
                }
                if (WIFEXITED(b) && !WEXITSTATUS(b)) {
                    pid_t tim_pid3 = fork();
                    if (tim_pid3 < 0) {
                        exit(1);
                    }
                    else if (tim_pid3 == 0) {
                        printf("%s", filename);
                        char argv_bek[200];
                        sprintf(argv_bek, "cd /home/rizqy/players/Bek && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", num_bek, filename);
                        char* argv[] = { "sh", "-c",argv_bek, NULL };
                        execvp("sh", argv);
                        perror("error");
                        exit(1);
                    }
                    else {
                        int c;
                        if (waitpid(tim_pid3, &c, 0) < 0) {
                            perror("error");
                            exit(1);
                        }
                        if (WIFEXITED(c) && !WEXITSTATUS(c)) {
                            pid_t tim_pid4 = fork();
                            if (tim_pid4 < 0) {
                                exit(1);
                            }
                            else if (tim_pid4 == 0) {
                                printf("%s", filename);
                                char argv_gldg[200];
                                sprintf(argv_gldg, "cd /home/rizqy/players/Gelandang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", num_gelandang, filename);
                                char* argv[] = { "sh", "-c",argv_gldg, NULL };
                                execvp("sh", argv);
                                perror("error");
                                exit(1);
                            }
                            else {
                                int d;
                                if (waitpid(tim_pid4, &d, 0) < 0) {
                                    perror("error");
                                    exit(1);
                                }
                                if (WIFEXITED(d) && !WEXITSTATUS(d)) {
                                    pid_t tim_pid5 = fork();
                                    if (tim_pid5 < 0) {
                                        exit(1);
                                    }
                                    else if (tim_pid5 == 0) {
                                        printf("%s", filename);
                                        char argv_striker[200];
                                        sprintf(argv_striker, "cd /home/rizqy/players/Penyerang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", num_penyerang, filename);
                                        char* argv[] = { "sh", "-c",argv_striker, NULL };
                                        execvp("sh", argv);
                                        perror("error");
                                        exit(1);
                                    }
                                    else {
                                        int e;
                                        waitpid(tim_pid5, &e, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

int main() {
    pid_t pid = fork();
    if (pid < 0) {
        return 1;
    }
    else if (pid == 0) {
        char* argv[] = { "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL };
        execvp("wget", argv);
        exit(1);
    }
    else {
        int a;
        waitpid(pid, &a, 0);
        if (WIFEXITED(a) && !WEXITSTATUS(a)) {
            pid_t pid2 = fork();
            if (pid2 < 0) {
                return 1;
            }
            else if (pid2 == 0) {
                char* argv[] = { "unzip", "players.zip", NULL };
                execvp("unzip", argv);
                exit(1);
            }
            else {
                int b;
                waitpid(pid2, &b, 0);
                if (WIFEXITED(b) && !WEXITSTATUS(b)) {
                    pid_t pid3 = fork();
                    if (pid3 < 0) {
                        return 1;
                    }
                    else if (pid3 == 0) {
                        char* argv[] = { "rm", "players.zip", NULL };
                        execvp("rm", argv);
                        exit(1);
                    }
                    else {
                        int c;
                        waitpid(pid3, &c, 0);
                        if (WIFEXITED(c) && !WEXITSTATUS(c)) {
                            pid_t pid4 = fork();
                            if (pid4 < 0) {
                                return 1;
                            }
                            else if (pid4 == 0) {
                                char* argv[] = { "sh", "-c", "find /home/rizqy/players -type f ! -name '*ManUtd*' -delete", NULL };
                                execvp("sh", argv);
                                exit(1);
                            }
                            else {
                                int d;
                                waitpid(pid4, &d, 0);
                                if (WIFEXITED(d) && !WEXITSTATUS(d)) {
                                    pid_t pid5 = fork();
                                    if (pid5 < 0) {
                                        return 1;
                                    }
                                    else if (pid5 == 0) {
                                        char* argv[] = { "sh", "-c", "mkdir -p /home/rizqy/players/Bek && mkdir -p /home/rizqy/players/Gelandang && mkdir -p /home/rizqy/players/Kiper && mkdir -p /home/rizqy/players/Penyerang", NULL };
                                        execvp("sh", argv);
                                        exit(1);
                                    }
                                    else {
                                        int e;
                                        waitpid(pid5, &e, 0);
                                        if (WIFEXITED(e) && !WEXITSTATUS(e)) {
                                            pid_t pid6 = fork();
                                            if (pid6 < 0) {
                                                return 1;
                                            }
                                            else if (pid6 == 0) {
                                                char* argv[] = { "sh", "-c", "cd /home/rizqy/players && mv *Bek*.png Bek/ && mv *Gelandang*.png Gelandang/ && mv *Kiper*.png Kiper/ && mv *Penyerang*.png Penyerang/", NULL };
                                                execvp("sh", argv);
                                                exit(1);
                                            }
                                            else {
                                                int f;
                                                waitpid(pid6, &f, 0);
                                                if (WIFEXITED(f) && !WEXITSTATUS(f)) {
                                                    pid_t pid7 = fork();
                                                    if (pid7 < 0) {
                                                        return 1;
                                                    }
                                                    else if (pid7 == 0) {
                                                        buatTim(3, 4, 3);
                                                    }
                                                    else {
                                                        int g;
                                                        waitpid(pid7, &g, 0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <curl/curl.h>
#include <wait.h>

int main() {
  char *url = "https://its.id/m/sisopmodul2d07";
  char *path = "./zoo_shift";
  char *file = "./zoo_shift/animals.zip";

  DIR *dir = opendir(path);

  if (dir) {
    printf("Zoo Shift Directory Already Exists\n");
    closedir(dir);
  } else {
    printf("Directory does not exist, creating folder ...\n");
    int status = mkdir(path, 0777);

    if (status == 0) {
      printf("Zoo Shift Directory Created\n");
    } else {
      printf("Zoo Shift Directory Failed to Create\n");
    }
  }

  //  Download file
  char command[200] = "wget -O ";
  strcat(command, file);
  strcat(command, " ");
  strcat(command, url);
  system(command);

  //  Extract all files from animals.zip using exec
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    int unzip_status = execlp("unzip", "unzip", "-q", file, "-d", path, (char*)NULL);
    if (unzip_status == -1) {
      perror("Error unzipping file");
      exit(1);
    }
  } else {
    while ((wait(&status)) > 0);
  }

  //  Create directories for each animal type
  char *animalTypes[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
  int animalTypesLength = sizeof(animalTypes) / sizeof(animalTypes[0]);

  for (int i = 0; i < animalTypesLength; i++) {
    char *animalType = animalTypes[i];
    char animalTypePath[200] = "./zoo_shift/";
    strcat(animalTypePath, animalType);
    DIR *animalTypeDir = opendir(animalTypePath);

    if (animalTypeDir) {
      printf("Directory %s already exists\n", animalType);
      closedir(animalTypeDir);
    } else {
      printf("Directory %s does not exist, creating folder ...\n", animalType);
      int status = mkdir(animalTypePath, 0777);

      if (status == 0) {
        printf("Directory %s created\n", animalType);
      } else {
        printf("Directory %s failed to create\n", animalType);
      }
    }
  }

  // Move all animal files to their respective animal type directories
  char file_names[100][100];
  int file_count = 0;

  struct dirent *de;
  DIR *dr = opendir("./zoo_shift/");

  if (dr == NULL) {
    printf("Could not open current directory" );
    return 0;
  }

  // get file names
  while ((de = readdir(dr)) != NULL) {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
      continue;
    }

    // skip if file name starts with "Hewan"
    if (strncmp(de->d_name, "Hewan", 5) == 0) {
      continue;
    }

    // skip if file name after dot is zip
    if (strcmp(strchr(de->d_name, '.') + 1, "zip") == 0) {
      continue;
    }

    strcpy(file_names[file_count], de->d_name);
    file_count++;

    printf("Found file: %s\n", de->d_name); 
  }

  closedir(dr);

  // get animal type from file name after underscore
  for (int i = 0; i < file_count; i++) {
    char *fileName = file_names[i];
    char *animalType = strchr(fileName, '_') + 1;
    animalType = strtok(animalType, ".");

    printf("Moving %s to %s ...\n", fileName, animalType);

    if (strcmp(animalType, "darat") == 0) {
      char command[200] = "mv ./zoo_shift/";
      strcat(command, fileName);
      strcat(command, ".jpg");
      strcat(command, " ./zoo_shift/HewanDarat/");
      system(command);
    } else if (strcmp(animalType, "amphibi") == 0) {
      char command[200] = "mv ./zoo_shift/";
      strcat(command, fileName);
      strcat(command, ".jpg");
      strcat(command, " ./zoo_shift/HewanAmphibi/");
      system(command);
    } else if (strcmp(animalType, "air") == 0) {
      char command[200] = "mv ./zoo_shift/";
      strcat(command, fileName);
      strcat(command, ".jpg");
      strcat(command, " ./zoo_shift/HewanAir/");
      system(command);
    }
  }

  // zip each animal type directory using exec
  for (int i = 0; i < animalTypesLength; i++) {
    char *animalType = animalTypes[i];
    char animalTypePath[200] = "./zoo_shift/";
    char destination[200] = "./zoo_shift/";
    strcat(destination, animalType);
    strcat(animalTypePath, animalType);

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
      int zip_status = execlp("zip", "zip", "-q", "-r", destination, animalTypePath, (char*)NULL);
      if (zip_status == -1) {
        perror("Error zipping file");
        exit(1);
      }
    } else {
      while ((wait(&status)) > 0);
    }
  }

  // remove all animal type directories
  for (int i = 0; i < animalTypesLength; i++) {
    char *animalType = animalTypes[i];
    char animalTypePath[200] = "./zoo_shift/";
    strcat(animalTypePath, animalType);

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
      int remove_status = execlp("rm", "rm", "-r", animalTypePath, (char*)NULL);
      if (remove_status == -1) {
        perror("Error removing directory");
        exit(1);
      }
    } else {
      while ((wait(&status)) > 0);
    }
  }

  // get random animal name
  srand(time(0));
  int randomIndex = rand() % file_count;
  char *randomAnimalName = file_names[randomIndex];
  char *animalName = strtok(randomAnimalName, "_");

  printf("\nRANDOM ANIMAL NAME: %s\n\n", animalName);
}
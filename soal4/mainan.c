#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>

int main(int argc, char *argv[]) {
  // check arguments
  if (argc != 5) {
    printf("[USAGE] ./banabil [hour] [minute] [second] [path]\n");
    exit(EXIT_FAILURE);
  }

  // handle argument
  int hour;
  int minute;
  int second;
  char path[200];

  if (strcmp(argv[1], "\*") == 0) {
    hour = -1;
  } else {
    hour = atoi(argv[1]);
  }

  if (strcmp(argv[2], "\*") == 0) {
    minute = -1;
  } else {
    minute = atoi(argv[2]);
  }

  if (strcmp(argv[3], "\*") == 0) {
    second = -1;
  } else {
    second = atoi(argv[3]);
  }

  strcpy(path, argv[4]);

  // create the daemon program
  pid_t pid, sid;

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    // get current time
    time_t current_time;
    struct tm *time_info;

    time(&current_time);
    time_info = localtime(&current_time);

    int current_hour = time_info->tm_hour;
    int current_minute = time_info->tm_min;
    int current_second = time_info->tm_sec;

    // determine sleep time according to the argument
    int sleep_time = 0;

    if (hour == -1) {
      if (minute == -1) {
        if (second == -1) {
          sleep_time = 1;
        } else {
          if (second > current_second) {
            sleep_time = second - current_second;
          } else {
            sleep_time = 60 - current_second + second;
          }
        }
      } else {
        if (second == -1) {
          if (minute > current_minute) {
            sleep_time = (minute - current_minute) * 60 + (60 - current_second);
          } else {
            sleep_time = (60 - current_minute) * 60 + (60 - current_second) + (minute * 60);
          }
        } else {
          if (minute > current_minute) {
            if (second > current_second) {
              sleep_time = (minute - current_minute) * 60 + (second - current_second);
            } else {
              sleep_time = (minute - current_minute) * 60 + (60 - current_second) + (second);
            }
          } else {
            if (second > current_second) {
              sleep_time = (60 - current_minute) * 60 + (second - current_second) + (minute * 60);
            } else {
              sleep_time = (60 - current_minute) * 60 + (60 - current_second) + (minute * 60) + second;
            }
          }
        }
      }
    } else {
      if (hour > current_hour) {
        if (minute > current_minute) {
          if (second > current_second) {
            sleep_time = (hour - current_hour) * 3600 + (minute - current_minute) * 60 + (second - current_second);
          } else {
            sleep_time = (hour - current_hour) * 3600 + (minute - current_minute) * 60 + (60 - current_second) + second;
          }
        } else {
          if (second > current_second) {
            sleep_time = (hour - current_hour) * 3600 + (60 - current_minute) * 60 + (second - current_second) + (minute * 60);
          } else {
            sleep_time = (hour - current_hour) * 3600 + (60 - current_minute) * 60 + (60 - current_second) + (minute * 60) + second;
          }
        }
      } else {
        if (minute == -1) {
          if (second == -1) {
            sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (60 - current_minute) * 60 + (60 - current_second);
          } else {
            if (second > current_second) {
              sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (60 - current_minute) * 60 + (second - current_second);
            } else {
              sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (60 - current_minute) * 60 + (60 - current_second) + second;
            }
          }
        } else {
          if (minute > current_minute) {
            if (second == -1) {
              sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (minute - current_minute) * 60 + (60 - current_second);
            } else {
              if (second > current_second) {
                sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (minute - current_minute) * 60 + (second - current_second);
              } else {
                sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (minute - current_minute) * 60 + (60 - current_second) + second;
              }
            }
          } else {
            if (second == -1) {
              sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (60 - current_minute) * 60 + (60 - current_second) + (minute * 60);
            } else {
              if (second > current_second) {
                sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (60 - current_minute) * 60 + (second - current_second) + (minute * 60);
              } else {
                sleep_time = (24 - current_hour) * 3600 + (hour * 3600) + (60 - current_minute) * 60 + (60 - current_second) + (minute * 60) + second;
              }
            }
          }
        }
      }
    }

    // child program to execute script

    pid_t grandchild_id;
    int status;

    grandchild_id = fork();

    if (grandchild_id < 0) {
      exit(1);
    }

    if (grandchild_id == 0) {
      chmod(path, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
      execl("/bin/sh", "sh", "-c", path, (char *)NULL);
    } else {
      while ((wait(&status)) > 0);
    }

    // child program to write log
    char command[200] = "echo 'Daemon running at: ";
    char str_hour[5];
    char str_minute[5];
    char str_second[5];

    sprintf(str_hour, "%d", current_hour);
    sprintf(str_minute, "%d", current_minute);
    sprintf(str_second, "%d", current_second);

    strcat(command, str_hour);
    strcat(command, ":");
    strcat(command, str_minute);
    strcat(command, ":");
    strcat(command, str_second);
    strcat(command, "\'");
    // using local directory
    strcat(command, " >> /home/nuna/sisop-praktikum-modul-2-2023-bs-d07/soal4/daemon_log.txt");

    pid_t log_id;
    int log_status;

    log_id = fork();

    if (log_id < 0) {
      exit(1);
    }

    if (log_id == 0) {
      execlp("sh", "sh", "-c", command, NULL);
    } else {
      while ((wait(&log_status)) > 0);
    }

    sleep(sleep_time);
  }
}
